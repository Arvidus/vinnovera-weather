import React, {Component} from 'react';
import './App.css'

import store from './store';

import {Provider} from "react-redux";
import {Home} from "./components/Home/Home";

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <link
                    rel="stylesheet"
                    href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
                    integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
                    crossOrigin="anonymous"
                />
                <div className="App">
                    <Home/>
                </div>
            </Provider>
        );
    }
}

export default App;
