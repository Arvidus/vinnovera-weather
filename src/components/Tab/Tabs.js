import React from 'react';

import './Tabs.css';
import store from '../../store'

import closeImg from '../../resources/close.png'

export class Tabs extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tabInfo: []
        };
        this.child = React.createRef();
    }

    componentDidMount() {
        store.subscribe(() => {
            this.setState({
                tabInfo: store.getState().saveSearchTabs
            });
        });
    }

    static translateMeteoIcon(iconData){

        let meteo = "";

        switch (iconData) {
            case "01d":
                meteo = "B";
                break;
            case "02d":
                meteo = "H";
                break;
            case "03d":
                meteo = "N";
                break;
            case "04d":
                meteo = "Y";
                break;
            case "09d":
                meteo = "R";
                break;
            case "10d":
                meteo = "Q";
                break;
            case "11d":
                meteo = "0";
                break;
            case "13d":
                meteo = "V";
                break;
            case "50d":
                meteo = "M";
                break;
            case "01n":
                meteo = "C";
                break;
            case "02n":
                meteo = "I";
                break;
            case "03n":
                meteo = "N";
                break;
            case "04n":
                meteo = "Y";
                break;
            case "09n":
                meteo = "R";
                break;
            case "10n":
                meteo = "Q";
                break;
            case "11n":
                meteo = "0";
                break;
            case "13n":
                meteo = "V";
                break;
            case "50n":
                meteo = "M";
        }

        return meteo;
    }

    static closeTab(e){
        store.dispatch({
            type: 'CLOSE_TAB',
            tabName: e.target.getAttribute("data")
        });
    }

    render() {
        const {
            tabInfo
        } = this.state;

        const tabList = tabInfo.map((place, i) => {
            return <div key={place.name} className="tabEntry fade-in"
                        style={{background: '#' + Math.random().toString(16).substr(-6)}}>
                <div className="icon">{Tabs.translateMeteoIcon(place.weather[0].icon)}</div>
                <div className="temp">
                    <div className="right">
                        <b className="tempValue">{place.main.temp}<b className="celsius">&#176;C</b></b><br/>
                        <b className="placeName">{place.name}</b>
                    </div>
                </div>
                <img onClick={Tabs.closeTab.bind(this)} data={place.name} src={closeImg} alt=""/>
            </div>
        });

        return (
            <div className="Tabs">
                {tabList}
            </div>
        );

    }
}
