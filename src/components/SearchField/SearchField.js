import React from 'react';

import './SearchField.css';

import store from '../../store';

export class SearchField extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            searchInput: "",
            tabInfo: []
        };
        this.child = React.createRef();
    }

    fetchItems(place) {
        fetch('https://api.openweathermap.org/data/2.5/weather?q=' + place +
            '&appid=fae523608f43a2c7e05c69a344aee1f0&units=metric', {}).then(res => res.json())
            .then(
                (result) => {
                    if (result[Object.keys(result)[0]] !== "404") {
                        store.dispatch({
                            type: 'ADD_TAB',
                            object: result
                        });
                    }
                    this.setState({
                        searchInput: ""
                    });
                },
                (error) => {
                    console.log(error);
                }
            );
    }

    componentDidMount() {
        this.fetchItems("Stockholm");
    }

    inputUpdate(e) {                                              // Method for updating input state for list filter
        this.setState({
            searchInput: e.target.value,
        })
    }

    keyPress(e) {
        if (e.keyCode === 13) {
            console.log(e.target.value);
            this.fetchItems(e.target.value);
        }
    }

    render() {
        const {
            searchInput
        } = this.state;

        return (

            <div className="SearchField">
                <div className="SearchFieldContent">
                    <b className="how">How's the Weather in...</b><br/>
                    <div className="inputField">
                        <b className="searchLabel">Location:</b>
                        <b className="addTab" onClick={() => {
                            this.fetchItems(searchInput)
                        }}>+</b>
                        <input value={searchInput} className="citySearchField"
                               onKeyDown={this.keyPress.bind(this)} onChange={this.inputUpdate.bind(this)}/>
                    </div>
                </div>
            </div>
        );

    }
}
