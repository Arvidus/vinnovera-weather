import React from 'react';
import './Home.css';

import {SearchField} from "../SearchField/SearchField";
import {Tabs} from "../Tab/Tabs";

import logo from '../../resources/vinnovera_lang_mot_vitt7596.png'

export class Home extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            testResults: [],
            testResultsLoaded: false
        };
        this.child = React.createRef();
    }

    componentDidMount() {

    }

    render() {
        return (

            <div className="Home fade-in">
                <img className="logo" src={logo} alt=""/>
                <SearchField/>
                <Tabs/>
                <img className="logoResp" src={logo} alt=""/>
            </div>
        );

    }
}

