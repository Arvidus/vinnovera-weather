export default function saveSearchTabs(state = [], action) {
    switch (action.type) {
        case 'ADD_TAB':
            let alreadySearched = false;
            for (let i = 0; i < state.length; i++) {
                if (state[i].name === action.object.name) {
                    alreadySearched = true;
                    break
                }
            }
            if (alreadySearched) {
                return state
            } else {
                return state.concat([action.object]);
            }
        case 'CLOSE_TAB':
            let newState = [...state];
            let index = 0;
            for (let i = 0; i < newState.length; i++) {
                if (newState[i].name === action.tabName) {
                    index = i;
                    break
                }
            }
            newState.splice(index, 1);
            return newState;
        default:
            return state
    }
}
