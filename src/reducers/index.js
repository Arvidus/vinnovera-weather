import { combineReducers } from 'redux'
import saveSearchTabs from './saveSearchTabs'

export default combineReducers({
    saveSearchTabs,
})